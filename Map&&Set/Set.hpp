#include"RBTree.hpp"


namespace gyj
{
	template<class K>
	class set
	{
		typedef K ValueType;
		
		struct KeyOfValue
		{
			const K& operator()(const ValueType& value)
			{
				return value;
			}
		};
		typedef RBTree<ValueType, KeyOfValue> Tree;
	public:
		typedef typename Tree::iterator iterator;
	public:
		set()
			:_t()
		{}



		iterator begin()
		{
			return _t.Begin();
		}

		iterator end()
		{
			return _t.End();
		}

		//�������
		size_t size()const
		{
			return _t.Size();
		}

		bool empty()const
		{
			return _t.Empty();
		}


		//�޸�
		pair<iterator, bool> insert(const ValueType& value)
		{
			return _t.Insert(value);
		}

		void swap(set<ValueType>& s)
		{
			_t.Swap(s._t);
		}

		void clear()
		{
			_t.clear();
		}

		iterator find(const K& key)
		{
			return _t.Find(key);
		}
	private:
		Tree _t;
	};
}

#include<string>
using namespace std;

void TestSet()
{
	gyj::set<string> s;
	s.insert("orange");
	s.insert("banana");
	s.insert("apple");
	s.insert("pear");

	cout << s.size() << endl;

	auto it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	auto ret = s.find("grape");
	if (ret == s.end())
	{
		s.insert("grape");
	}

	for (auto& e : s)
	{
		cout << e << " ";
	}
	cout << endl;

	s.clear();
	if (s.empty())
	{
		cout << "Yes" << endl;
	}
	else
	{
		cout << "No" << endl;
	}
}