#include "Map.hpp"
#include "Set.hpp"

int main()
{
	cout << "Map-Test" << endl;
	TestMap();

	cout << endl << "Set-Test" << endl;
	TestSet();
	_CrtDumpMemoryLeaks();
	return 0;
}