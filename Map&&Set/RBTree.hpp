#pragma once
#include<iostream>

using namespace std;

enum Color { RED, BLACK };



template<class T>
struct RBTreeNode
{
	typedef RBTreeNode<T> Node;

	RBTreeNode(const T& value = T(), Color color = RED)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _value(value)
		, _color(color)
	{}

	Node* _left;
	Node* _right;
	Node* _parent;
	T _value;
	Color _color;
};

////////////////////迭代器实现//////////////////////////
template<class T, class Ref, class Ptr>
struct RBTreeIterator
{
	typedef RBTreeNode<T> Node;
	typedef RBTreeIterator<T, Ref, Ptr> Self;

	RBTreeIterator(Node* pNode = nullptr)
		:_pNode(pNode)
	{}

	//提供与指针类似的操作
	Ref operator*()
	{
		return _pNode->_value;
	}

	Ptr operator->()
	{
		return &(operator*());
	}

	//前置++ && 后置++
	Self& operator++()
	{
		Increament();
		return *this;
	}
	Self operator++(int)
	{
		Self tmp(*this);
		Increament();
		return tmp;
	}
	//前置-- && 后置--
	Self& operator--()
	{
		Decreament();
		return *this;
	}

	Self operator--(int)
	{
		Self tmp(*this);
		Decreament();
		return tmp;
	}

	bool operator==(const Self& s) const
	{
		return _pNode == s._pNode;
	}
	bool operator!=(const Self& s) const
	{
		return _pNode != s._pNode;
	}

	//实现迭代器“前后”移动的方法
	void Increament()
	{
		if (_pNode->_right)
		{
			//_pNode的右子树存在，就到右子树中找最小的，即最左侧的结点
			_pNode = _pNode->_right;
			while (_pNode->_left)
			{
				_pNode = _pNode->_left;
			}
		}
		else
		{
			//右子树不存在，一直向上找，直到找到第一个双亲与_pNode不是右孩子的关系
			Node* parent = _pNode->_parent;
			while (_pNode == parent->_right)
			{
				_pNode = parent;
				parent = parent->_parent;
			}
			//注意特殊情况,根节点没有右子树的情况
			if (_pNode->_right != parent)
			{
				_pNode = parent;
			}
		}
	}
	void Decreament()
	{
		//考虑迭代器在end的位置时，对其--，应该指向整棵红黑树的最右侧结点
		if (RED == _pNode->_color && _pNode->_parent->_parent == _pNode)
		{
			_pNode = _pNode->_right;
		}
		//对于其他位置
		else if (_pNode->_left)
		{
			//先到该节点的左子树中找最右侧的
			_pNode = _pNode->_left;
			while (_pNode->_right)
			{
				_pNode = _pNode->_right;
			}
		}
		else
		{
			Node* parent = _pNode->_parent;
			while (parent->_left == _pNode)
			{
				_pNode = parent;
				parent = parent->_parent;
			}
			_pNode = parent;
		}
	}

	Node* _pNode;
};

template<class T, class KeyOfValue>
class RBTree
{
	typedef RBTreeNode<T> Node;
public:
	typedef RBTreeIterator<T, T&, T*> iterator;
public:
	RBTree()
	{
		_head = new Node();
		_head->_left = _head;
		_head->_right = _head;
		_head->_parent = nullptr;
		_size = 0;
	}

	~RBTree()
	{
		Destroy(GetRoot());
		delete _head;
		_head = nullptr;
		_size = 0;
	}
	///////////////////////迭代器////////////////////////////
	iterator Begin()
	{
		return iterator(_head->_left);
	}

	iterator End()
	{
		return iterator(_head);
	}

	pair<iterator, bool> Insert(const T& value)
	{
		Node* & root = GetRoot();
		Node* newNode = nullptr;
		if (nullptr == root)
		{
			//是一棵只有头节点的空树
			root = new Node(value, BLACK);
			root->_parent = _head;
			_head->_parent = root;//更新头节点的_parent域??????
			newNode = root;
		}
		else//树不空
		{
			//寻找插入位置
			Node* cur = root;
			Node* parent = _head;
			KeyOfValue kofv;
			while (cur)
			{
				parent = cur;
				if (kofv(value) > kofv(cur->_value))
				{
					cur = cur->_right;
				}
				else if (kofv(value) < kofv(cur->_value))
				{
					cur = cur->_left;
				}
				else
				{
					return make_pair(iterator(cur), false);
				}
			}
			cur = new Node(value);
			newNode = cur;
			if (kofv(value) > kofv(parent->_value))
			{
				parent->_right = cur;
			}
			else
			{
				parent->_left = cur;
			}
			cur->_parent = parent;

			//判断插入该节点后是否违反了红黑树的相关性质
			//由于新节点默认颜色是红色，因此如果违反了红黑树的性质，一定是性质三：即是否存在红色节点连在一起的情况
			//如果说parent的结点颜色为红色，那就违反了性质三
			while (_head != parent && RED == parent->_color)//注意判断条件
			{
				Node* grandFather = parent->_parent;//grandFather一定存在
				//parent存在且为红，则parent一定不会是跟根节点，因此parent一定有双亲

				/*
				存在3种情况
				情况1：叔叔结点存在且为红
				情况2：叔叔结点为空 || 叔叔节点存在且为黑 && cur为双亲的左孩子
				情况3：叔叔结点为空 || 叔叔节点存在且为黑 && cur为双亲的右孩子
				可以将以双亲为根节点的子树先左单旋，然后交换双亲结点和cur结点---》此时转变为情况2解决
				*/
				if (parent == grandFather->_left)
				{
					Node* uncle = grandFather->_right;
					if (uncle && RED == uncle->_color)
					{
						//叔叔结点存在且为红
						//将双亲和叔叔结点改为黑，祖父结点改为红，继续向上跟新
						parent->_color = BLACK;
						uncle->_color = BLACK;
						grandFather->_color = RED;
						cur = grandFather;
						parent = cur->_parent;
					}
					else
					{
						//叔叔结点为空 || 叔叔节点存在且为黑
						//情况3
						if (cur == parent->_right)
						{
							//先进行左单旋，再交换双亲与cur结点--》转变为情况2
							RotateLeft(parent);
							swap(parent, cur);
						}
						//情况2处理
						//先将祖父和双亲结点颜色交换，然后再对以grandFather为根节点的树进行右单旋
						grandFather->_color = RED;
						parent->_color = BLACK;
						RotateRight(grandFather);
					}
				}
				else
				{
					//此时，parent是grandFather的右孩子，处理方式与上面的相反即可
					Node* uncle = grandFather->_left;
					if (uncle && RED == uncle->_color)
					{
						//叔叔结点存在且为红
						parent->_color = BLACK;
						uncle->_color = BLACK;
						grandFather->_color = RED;
						cur = grandFather;
						parent = cur->_parent;
					}
					else
					{
						//叔叔结点不存在 || 叔叔结点存在且为黑
						//情况三的反情况
						if (cur = parent->_left)
						{
							//通过对parent这棵树的右单旋将情况三转换为情况二
							RotateRight(parent);
							swap(parent, cur);
						}

						//按照情况2的反情况处理
						//将parent的颜色置为黑，祖父的颜色置为红
						parent->_color = BLACK;
						grandFather->_color = RED;
						//将祖父结点为根的子树进行左单旋
						RotateLeft(grandFather);
					}
				}
			}
		}
		//更新_head的左右指针域
		//左指针域指向整棵树的最左侧结点
		_head->_left = MostLeft();
		//右指针域指向整棵树的最右侧结点
		_head->_right = MostRight();
		root->_color = BLACK;//根节点始终为黑色
		_size++;

		return make_pair(iterator(newNode), true);
	}

	///////////find//////////////////
	iterator Find(const T& value)
	{
		Node* cur = GetRoot();
		KeyOfValue kofv;
		while (cur)
		{
			if (kofv(value) == kofv(cur->_value))
			{
				return iterator(cur);
			}
			else if (kofv(value) > kofv(cur->_value))
			{
				cur = cur->_right;
			}
			else
			{
				cur = cur->_left;
			}
		}
		return End();
	}
	

	size_t Size() const
	{
		return _size;
	}

	bool Empty() const
	{
		return _size == 0;
	}

	void Swap(RBTree<T, KeyOfValue>& t)
	{
		swap(_head, t._head);	
	}

	void clear()
	{
		Destroy(GetRoot());
		_size = 0;
	}

	/////////////////////验证RBTree///////////////////////////////////////

	//是一个二叉搜索树，中序遍历是有序的
	void InOrder()
	{
		InOrder(GetRoot());
		cout << endl;
	}

	bool IsValidRBTree()
	{
		Node* root = GetRoot();
		if (nullptr == root)
		{
			return true;
		}
		//判断性质2：根节点必须是黑色
		if (RED == root->_color)
		{
			cout << "违反了性质二：根节点必须为黑色" << endl;
			return false;
		}

		//求得任意一条路径中黑色节点个数，为了简单，这里求最左侧一条路径
		Node* cur = root;
		size_t blackCount = 0;
		while (cur)
		{
			if (cur->_color == BLACK)
			{
				blackCount++;
			}
			cur = cur->_left;
		}
		size_t pathBlackCount = 0;

		return IsValidRBTree(root, blackCount, pathBlackCount);
	}


private:
	void RotateRight(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
		{
			subLR->_parent = parent;
		}
		subL->_right = parent;

		//更新parent&&subL的_parent域
		Node* pparent = parent->_parent;
		parent->_parent = subL;
		subL->_parent = pparent;
		if (_head == pparent)
		{
			_head->_parent = subL;
		}
		else
		{
			if (pparent->_left == parent)
			{
				pparent->_left = subL;
			}
			else
			{
				pparent->_right = subL;
			}
		}
	}

	void RotateLeft(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;
		if (subRL)
		{
			subRL->_parent = parent;
		}
		subR->_left = parent;

		//更新parent和subR的_parent域
		Node* pparent = parent->_parent;
		parent->_parent = subR;
		subR->_parent = pparent;
		if (_head == pparent)
		{
			_head->_parent = subR;
		}
		else
		{
			if (pparent->_left == parent)
			{
				pparent->_left = subR;
			}
			else
			{
				pparent->_right = subR;
			}
		}
	}
	Node* MostLeft()
	{
		Node* cur = GetRoot();
		if (nullptr == cur)
		{
			return _head;
		}
		while (cur->_left)
		{
			cur = cur->_left;
		}
		return cur;
	}


	Node* MostRight()
	{
		Node* cur = GetRoot();
		if (nullptr == cur)
		{
			return _head;
		}
		while (cur->_right)
		{
			cur = cur->_right;
		}
		return cur;
	}


	Node* & GetRoot()
	{
		return _head->_parent;
	}
	void Destroy(Node*& root)
	{
		if (root)
		{
			Destroy(root->_left);
			Destroy(root->_right);
			delete root;
			root = nullptr;
		}
	}


	/////////////////验证RBTree////////////////////////
	void InOrder(Node* root)
	{
		if (root)
		{
			InOrder(root->_left);
			cout << root->_value << " ";
			InOrder(root->_right);
		}
	}

	bool IsValidRBTree(Node* root, const size_t& blackCount, size_t pathBlackCount)
	{
		if (nullptr == root)
		{
			return true;
		}
		//统计单条路径中黑色结点的个数
		if (BLACK == root->_color)
		{
			pathBlackCount++;
		}

		// 检测性质3
		/////////////////////////////////////////////////////////////////
		Node* parent = root->_parent;
		if (_head != parent &&
			RED == root->_color && RED == parent->_color)
		{
			cout << "违反了性质3：不能有连在一起的红色节点" << endl;
			return false;
		}
		///////////////////////////////////////////////////////////////

		if (nullptr == root->_left && nullptr == root->_right)
		{
			//走到了某一条路径的末尾
			if (pathBlackCount != blackCount)
			{
				cout << "违反了性质4：每条路径中黑色节点个数要相同" << endl;
				cout << blackCount << "<--->" << pathBlackCount << endl;
				return false;
			}

		}

		return IsValidRBTree(root->_left, blackCount, pathBlackCount) &&
			IsValidRBTree(root->_right, blackCount, pathBlackCount);
	}
private:
	Node* _head;
	size_t _size;

};



#if 0

struct KeyOfValue
{
	const int& operator()(const int& value)
	{
		return value;
	}
};

void TestRBTree()
{
	int a[] = { 5, 3, 4, 1, 7, 8, 2, 6, 0, 9 };
	RBTree<int,KeyOfValue> t;
	for (auto e : a)
		t.Insert(e);

	t.InOrder();
	if (t.IsValidRBTree())
	{
		cout << "t is valid RBTree!!!" << endl;
	}
	else
	{
		cout << "t is invalid RBTree!!!" << endl;
	}

	//RBTreeIterator<int, int*, int&> iter = t.Begin();
	auto iter = t.Begin();
	while (iter != t.End())
	{
		cout << *iter << " ";
		++iter;
	}
	cout << endl;

	auto it = t.End();
	while (it != t.Begin())
	{
		--it;
		cout << *it << " ";
	}
	cout << endl;
	
}
#endif
