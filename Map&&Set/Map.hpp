#include"RBTree.hpp"

namespace gyj
{
	template<class K, class V>
	class map
	{
		typedef pair<K, V> ValueType;
		
		struct KeyOfValue
		{
			const K& operator()(const ValueType& value)
			{
				return value.first;
			}
		};
		typedef RBTree<ValueType, KeyOfValue> Tree;
	public:
		typedef typename Tree::iterator iterator;
	public:
		map()
			:_t()
		{}

	

		iterator begin()
		{
			return _t.Begin();
		}

		iterator end()
		{
			return _t.End();
		}

		//容量相关
		size_t size()const
		{
			return _t.Size();
		}

		bool empty()const
		{
			return _t.Empty();
		}

		//元素访问相关
		V& operator[](const K& key)
		{
			return (*(_t.Insert(make_pair(key, V())).first)).second;
		}

		//修改
		pair<iterator, bool> insert(const ValueType& value)
		{
			return _t.Insert(value);
		}

		void swap(map<K, V>& m)
		{
			_t.Swap(m._t);
		}

		void clear()
		{
			_t.clear();
		}

		iterator find(const K& key)
		{
			return _t.Find(make_pair(key, V()));
		}
	private:
		Tree _t;
	};
}

#include<string>
using namespace std;

void TestMap()
{
	gyj::map<string, string> m;
	m.insert(make_pair("orange", "橘子"));
	m.insert(make_pair("banana", "香蕉"));
	m.insert(make_pair("apple", "苹果"));
	m.insert(make_pair("pear", "梨"));

	cout << m.size() << endl;

	auto it = m.begin();
	while (it != m.end())
	{
		cout << it->first << "-->" << it->second << endl;
		++it;
	}
	cout << m["orange"] << endl;
	m["orange"] = "橙子";
	cout << m["orange"] << endl;

	auto ret = m.find("grape");
	if (ret == m.end())
	{
		m["grape"] = "葡萄";
	}

	for (auto& e : m)
	{
		cout << e.first << "-->" << e.second << endl;
	}

	m.clear();
	if (m.empty())
	{
		cout << "Yes" << endl;
	}
	else
	{
		cout << "No" << endl;
	}
}