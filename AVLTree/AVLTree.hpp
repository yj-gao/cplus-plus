#pragma once


#include<iostream>

using namespace std;

template<class T>
struct AVLTreeNode
{
	typedef AVLTreeNode<T> Node;
	AVLTreeNode(const T& value = T())
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _value(value)
		, _bf(0)
	{}

	Node* _left;
	Node* _right;
	Node* _parent;
	T _value;
	int _bf;//平衡因子
};

template<class T>
class AVLTree
{
	typedef AVLTreeNode<T> Node;
public:
	AVLTree()
		:_root(nullptr)
	{}

	~AVLTree()
	{
		Destory(_root);
	}

	//申明：模拟实现的AVL树中结点的值是唯一的
	//（如果遇到值相同结点，直接返回false，表示插入失败）
	bool Insert(const T& value)
	{
		if (nullptr == _root)
		{
			//这是一棵空树，直接插入结点即可
			_root = new Node(value);
			return true;
		}

		//1、按照二叉搜索树的方式查找结点的插入位置
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_value == value)
			{
				return false;
			}
			else if (cur->_value > value)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				parent = cur;
				cur = cur->_right;
			}
		}
		cur = new Node(value);
		if (value > parent->_value)
		{
			parent->_right = cur;
		}
		else
		{
			parent->_left = cur;
		}
		cur->_parent = parent;

		//2、跟新结点的平衡因子
		while (parent)
		{

			if (cur == parent->_left)
			{
				parent->_bf--;
			}
			else
			{
				parent->_bf++;
			}
			//判断parent的平衡因子是否满足AVL的性质
			if (0 == parent->_bf)
			{
				//以parent为根的二叉树高度没有发生变化，不回影响到上层的其他节点的平衡因子
				//因此，结束更新
				break;
			}
			else if (-1 == parent->_bf || 1 == parent->_bf)
			{
				//此时以parent为根的二叉树高度增加了，需要继续向上跟新
				cur = parent;
				parent = cur->_parent;
			}
			else
			{
				// parent的平衡因子可能是2或者-2，违反了AVL树的特性
				// 就需要对以parent为根的二叉树进行旋转处理
				if (2 == parent->_bf)
				{
					// parent的右子树高，最终需要左旋转
					if (1 == cur->_bf)
						RotateLeft(parent);
					else
						RotateRL(parent);
				}
				else
				{
					// parent的左子树高，最终需要右旋转
					if (-1 == cur->_bf)
						RotateRight(parent);
					else
						RotateLR(parent);
				}

				break;
			}
		}
		return true;
	}

	void InOrder()
	{
		InOrder(_root);
		cout << endl;
	}

	bool IsAVLTree()
	{
		return _IsAVLTree(_root);
	}

private:
	void RotateRight(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		subL->_right = parent;
		if (subLR)
		{
			subLR->_parent = parent;
		}
		//更新subL和parent的双亲
		Node* pparent = parent->_parent;
		parent->_parent = subL;
		subL->_parent = pparent;

		//处理pparent
		if (nullptr == pparent)
		{
			_root = subL;
		}
		else
		{
			if (pparent->_left == parent)
			{
				pparent->_left = subL;
			}
			else
			{
				pparent->_right = subL;
			}
		}

		//跟新subL和parent的平衡因子
		subL->_bf = parent->_bf = 0;

	}

	void RotateLeft(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;
		if (subRL)
		{
			subRL->_parent = parent;
		}
		subR->_left = parent;

		//更新parent && subR的双亲
		Node* pparent = parent->_parent;
		parent->_parent = subR;
		subR->_parent = pparent;

		//处理pparent
		if (nullptr == pparent)
		{
			_root = subR;
		}
		else
		{
			if (pparent->_left == parent)
			{
				pparent->_left = subR;
			}
			else
			{
				pparent->_right = subR;
			}
		}
		//跟新subR && parent的平衡因子
		subR->_bf = parent->_bf = 0;
	}
	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		int bf = subLR->_bf;


		RotateLeft(parent->_left);
		RotateRight(parent);

		if (1 == bf)
		{
			subL->_bf = -1;
		}
		else if (-1 == bf)
		{
			parent->_bf = 1;
		}
	}
	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		int bf = subRL->_bf;

		RotateRight(parent->_right);
		RotateLeft(parent);

		if (1 == bf)
			parent->_bf = -1;
		else if (-1 == bf)
			subR->_bf = 1;
	}

	void Destory(Node*& root)//注意：需要给引用
	{
		if (root)
		{
			Destory(root->_left);
			Destory(root->_right);
			delete root;
			root = nullptr;
		}
	}

	/////////////////验证AVL树////////////////////
	void InOrder(Node* root)
	{
		if (root)
		{
			InOrder(root->_left);
			cout << root->_value << " ";
			InOrder(root->_right);
		}
	}
	int GetHigh(Node* root)
	{
		if (nullptr == root)
			return 0;

		int leftHigh = GetHigh(root->_left);
		int rightHigh = GetHigh(root->_right);
		return leftHigh > rightHigh ? leftHigh + 1 : rightHigh + 1;
	}
	bool _IsAVLTree(Node* root)
	{
		if (nullptr == root)
		{
			return true;
		}
		int leftHigh = GetHigh(root->_left);
		int rightHigh = GetHigh(root->_right);
		if (rightHigh - leftHigh != root->_bf || abs(root->_bf) > 1)
		{
			cout << "Node:" << root->_value << endl;
			cout << rightHigh - leftHigh << " " << root->_bf << endl;
			return false;
		}
		// 根的左子树 和 根的右子树
		return _IsAVLTree(root->_left) &&
			_IsAVLTree(root->_right);
	}
private:
	Node* _root;
};


void TestAVLTree()
{
	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
	AVLTree<int> t;
	for (auto e : a)
		t.Insert(e);

	t.InOrder();

	if (t.IsAVLTree())
		cout << "t is valid AVLTree" << endl;
	else
		cout << "t is invalid AVLTree" << endl;
}