#pragma once

#include<iostream>
#include<string>
#include<vector>
#include<map>
#include<queue>

using namespace std;

namespace Matrix 
{
	template <class V, class W, W MAX_W = INT_MAX, bool Direction = false>
	class Graph
	{
	public:
		Graph(const V* arr, int size)
		{
			_vertexs.resize(size);
			for (int i = 0; i < size; ++i)
			{
				_indexMap[arr[i]] = i;
				_vertexs[i] = arr[i];
			}
			_matrix.resize(size);
			for (int i = 0; i < size; ++i)
			{
				_matrix[i].resize(size, MAX_W);
			}
		}

		//根据顶点计算对应下标
		size_t GetVertexIndex(const V& vertex)
		{
			if (_indexMap.find(vertex) != _indexMap.end())
			{
				return _indexMap[vertex];
			}
			else
			{
				cout << "顶点不存在" << endl;
				throw invalid_argument("顶点不存在");
				return -1;
			}
		}

		//构建顶点之间的关系
		void AddEdge(const V& src, const V& dst, const W& w)
		{
			size_t srci = GetVertexIndex(src);
			size_t dsti = GetVertexIndex(dst);
			_AddEdge(srci,dsti,w);
		}

		void _AddEdge(size_t srci, size_t dsti, const W& w)
		{
			_matrix[srci][dsti] = w;
			if (Direction == false)
			{
				//无向图，添加另一条边
				_matrix[dsti][srci] = w;
			}
		}

		//打印图结构
		void Print()
		{
			//打印顶点
			for (size_t i = 0; i < _vertexs.size(); ++i)
			{
				cout << "[" << i << "] ->" << _vertexs[i] << endl;
			}
			cout << endl;

			//打印邻接矩阵
			cout << "  ";
			for (size_t i = 0; i < _vertexs.size(); ++i)
			{
				printf("%4d", i);
			}
			cout << endl;

			for (size_t i = 0; i < _matrix.size(); ++i)
			{
				cout << i << "  ";
				for (size_t j = 0; j < _matrix[i].size(); j++)
				{
					if (_matrix[i][j] == MAX_W)
					{
						printf("%4c", '*');
					}
					else
					{
						printf("%4d", _matrix[i][j]);
					}
				}
				cout << endl;
			}
		}

		//广度优先遍历
		void BFS(const V& vertex)
		{
			size_t v_index = GetVertexIndex(vertex);
			queue<int> que;
			vector<bool> visited(_vertexs.size(), false);

			que.push(v_index);
			visited[v_index] = true;
			while (!que.empty())
			{
				int front = que.front();
				que.pop();
				cout << front << ":" << _vertexs[front] << endl;
				for (size_t i = 0; i < _vertexs.size(); i++)
				{
					if (_matrix[front][i] != MAX_W)
					{
						if (visited[i] == false)
						{
							que.push(i);
							visited[i] = true;
						}
					}
				}
			}
			cout << endl;
		}
		//深度优先遍历
		void DFS(const V& vertex)
		{
			size_t v_index = GetVertexIndex(vertex);
			vector<bool> visited(_vertexs.size(), false);
			_DFS(v_index, visited);
		}

		void _DFS(size_t index, vector<bool>& visited)
		{
			//访问该顶点
			cout << index << ":" << _vertexs[index] << endl;
			visited[index] = true;
			for (size_t i = 0; i < _matrix.size(); i++)
			{
				if (_matrix[index][i] != MAX_W && visited[i] == false)
				{
					_DFS(i, visited);
				}
			}
		}

	private:
		//顶点集合
		vector<V> _vertexs;	
		//顶点映射下标关系
		map<V, int> _indexMap;
		//邻接矩阵
		vector<vector<W>> _matrix;
	};

	void TestGraph()
	{
		string vec[] = {"张三", "李四","王五","赵六","田七"};
		Graph<string, int> g(vec, 5);

		g.AddEdge("张三", "李四", 100);
		g.AddEdge("张三", "王五", 200);
		g.AddEdge("王五", "赵六", 30);
		g.AddEdge("王五", "田七", 30);
		g.Print();

		g.BFS("张三");
		g.DFS("张三");
	}
};