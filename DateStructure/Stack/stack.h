#pragma once

#include <iostream>

using namespace std;

typedef  int SDataType;

class Stack
{
public:
	Stack();
	~Stack();
	void StackPush(SDataType x);
	void StackPop();
	SDataType StackTop();
	int StackSize();
	int StackEmpty();//1:�� 0:�ǿ�
private:
	void StackExpand();
private:
	SDataType* _array;
	int _size;
	int _capacity;
};

//���Ժ���
void TestStack();