#include "stack.h"

Stack::Stack()
{
	_array = (SDataType*)malloc(sizeof(SDataType)* 3);
	if (nullptr == _array)
	{
		perror("malloc");
		return;
	}
	_size = 0;
	_capacity = 3;
}

void Stack::StackExpand()
{
	//1、申请新空间（这里按照原空间的2倍申请）
	int newCapacity = _capacity * 2;
	SDataType* tmp = (SDataType*)malloc(sizeof(SDataType)*newCapacity);
	if (nullptr == tmp)
	{
		perror("malloc");
		return;
	}
	//2、将旧空间的值copy的新空间
	for (int i = 0; i < _capacity; i++)
	{
		tmp[i] = _array[i];
	}
	//3、释放旧空间
	free(_array);
	//4、使用新空间
	_array = tmp;
	_capacity = newCapacity;
}

Stack::~Stack()
{
	if (_array)
	{
		free(_array);
		_size = 0;
		_capacity = 0;
	}
}

void Stack::StackPush(SDataType x)
{
	if (_size == _capacity)
	{
		//扩容
		StackExpand();
	}
	_array[_size++] = x;
}
void Stack::StackPop()
{
	if (!StackEmpty())
	{
		_size--;
	}
}
SDataType Stack::StackTop()
{
	if (StackEmpty())
	{
		perror("StackEmpty!");
		return -1;
	}
	return _array[_size - 1];
}
int Stack::StackSize()
{
	return _size;
}
int Stack::StackEmpty()//1:空 0:非空
{
	return 0 == _size;
}

void TestStack()
{
	Stack s;
	s.StackPush(1);
	s.StackPush(2);
	s.StackPush(3);
	s.StackPush(4);
	s.StackPush(5);
	s.StackPush(6);

	cout << s.StackSize() << endl;
	cout << s.StackTop() << endl;

	s.StackPop();
	s.StackPop();

	cout << s.StackSize() << endl;
	cout << s.StackTop() << endl;

}
