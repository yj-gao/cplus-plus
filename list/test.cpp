#include<iostream>
#include<list>

using namespace std;

void TestConstructor()
{
	list<int> l1;
	list<int> l2(5, 90);

	int array[] = { 1, 2, 4, 5, 5, 6, 56, 78, 65, 90 };

	list<int> l3(array,array+sizeof(array) / sizeof(array[0]));

	list<int> l4(l2);
	l1 = l4;
}

void TestIterators1()
{
	int array[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
	list<int> l1(array, array + sizeof(array) / sizeof(array[0]));

	const list<int>l2(array, array + sizeof(array) / sizeof(array[0]));
	//测试begin&&end
	cout << "Before:" << endl;
	for (auto iter = l1.begin(); iter != l1.end(); ++iter)
	{
		cout << *iter << " ";
	}
	cout << endl;

	auto it1 = l1.begin();
	*it1 = 100;
	auto it2 = l2.begin();
	// *it2 = 100;//错误写法，l2被const修饰，因此只能调用const成员方法，因此也就无法通过迭代器修改元素的值
	++it2;

	cout << "After:" << endl;
	for (auto iter = l1.begin(); iter != l1.end(); ++iter)
	{
		cout << *iter << " ";
	}
	cout << endl;
	//测试rbegin && rend
	cout << "rbegin && rend:" << endl;
	for (auto iter = l1.rbegin(); iter != l1.rend(); ++iter)
	{
		cout << *iter << " ";
	}
	cout << endl;
}

void TestIterators2()
{
	int array[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
	list<int> l1(array, array + sizeof(array) / sizeof(array[0]));
	const list<int>l2(array, array + sizeof(array) / sizeof(array[0]));
	//测试cbegin&&cend
	cout << "Before:" << endl;
	for (auto iter = l1.cbegin(); iter != l1.cend(); ++iter)
	{
		cout << *iter << " ";
	}
	cout << endl;

	auto it1 = l1.cbegin();
	// *it1 = 100;//错误写法，cbegin返回值指向的元素不可被修改


	//测试crbegin && crend
	cout << "rbegin && rend:" << endl;
	for (auto iter = l1.crbegin(); iter != l1.crend(); ++iter)
	{
		cout << *iter << " ";
	}
	cout << endl;
}

void TestCapacity()
{
	int array[] = { 1, 2, 4, 5, 5, 6, 56, 78, 65, 90 };
	list<int> l(array, array + sizeof(array) / sizeof(array[0]));

	cout << l.size() << endl;
	if (l.empty())
	{
		cout << "l is empty" << endl;
	}
	else
	{
		cout << "l is not empty" << endl;
	}
}

void TestElementAccess()
{
	int array[] = { 1, 2, 4, 5, 5, 6, 56, 78, 65, 90 };
	list<int> l(array, array + sizeof(array) / sizeof(array[0]));
	cout << l.front() << endl;
	cout << l.back() << endl;
}

void TestAssign()
{
	int array[] = { 1, 2, 4, 5, 5, 6, 56, 78, 65, 90 };
	list<int> l(array, array + sizeof(array) / sizeof(array[0]));

	int temp[] = { 100, 200, 300 };
	l.assign(temp, temp + sizeof(temp) / sizeof(temp[0]));

	l.assign(10, -100);
}

void TestModifiers()
{
	list<int> l;
	int array[] = { 1, 2, 3, 4, 5, 6 };
	l.insert(l.begin(), 100);
	l.insert(l.end(), 2, -10);
	auto iter = l.begin();
	iter++;
	l.insert(iter, array, array + sizeof(array) / sizeof(array[0]));

	auto it = l.end();
	
	l.erase(--it);
	l.erase(l.begin(), l.end());

}

void TestSwap()
{
	int array[] = { 1, 2, 4, 5, 5, 6, 56, 78, 65, 90 };
	list<int> l1(array, array + sizeof(array) / sizeof(array[0]));

	list<int> l2(3, 100);
	l1.swap(l2);
	l1.clear();

}
int main()
{
	//TestConstructor();
	//list<int> l1(3, 10);
	//list<int> l2 = l1;
	//list<int> l3;
	//l3 = l1;
	//TestIterators1();
	//TestIterators2();
	//TestCapacity();
	//TestElementAccess();
	//TestAssign();
	//TestModifiers();
	TestSwap();
	return 0;
}
