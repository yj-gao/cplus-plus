#include<iostream>

using namespace std;

namespace gyj
{
	//链表中的每一个结点
	template<class T>
	class ListNode
	{
	public:
		ListNode<T>* next;
		ListNode<T>* prev;
		T data;
		ListNode(const T& value = T())
			:next(nullptr)
			, prev(nullptr)
			, data(value)
		{}
	};

	//正向迭代器
	template<class T,class Ref,class Ptr>
	class ListIterator
	{
	public:
		typedef ListNode<T> Node;
		typedef Ref Reference;
		typedef Ptr Pointer;


		typedef ListIterator<T, Ref, Ptr> Self;
	public:
		ListIterator(Node* pNode = nullptr)
			:_pNode(pNode)
		{}

		//解引用
		Ref operator*()
		{
			return _pNode->data;
		}
		//->操作（只有当T是自定义类型才有意义）
		Ptr operator->()
		{
			return &_pNode->data;
		}
		//前置++
		Self& operator++()
		{
			_pNode = _pNode->next;
			return *this;
		}
		//后置++
		Self operator++(int)
		{
			Self tmp(*this);
			_pNode = _pNode->next;
			return tmp;
		}

		//前置--
		Self& operator--()
		{
			_pNode = _pNode->prev;
			return *this;
		}
		//后置--
		Self operator--(int)
		{
			Self tmp(*this);
			_pNode = _pNode->prev;
			return tmp;
		}

		//比较
		bool operator==(const Self& it)
		{
			return _pNode == it._pNode;
		}
		bool operator!=(const Self& it)
		{
			return _pNode != it._pNode;
		}
	public:
		Node* _pNode;
	};

	//反向迭代器（就是对正向迭代器的一种重新包装）
	template<class Iterator>
	class ListReverseIterator
	{
	public:
		typedef typename Iterator::Reference Reference;
		typedef typename Iterator::Pointer Pointer;
		typedef ListReverseIterator<Iterator> Self;

		ListReverseIterator(Iterator it)
			:_it(it)
		{}

		Reference operator*()
		{
			auto tmp = _it;
			--tmp;
			return *tmp;
		}

		Pointer operator->()
		{
			return &(operator*());
		}
		Self& operator++()
		{
			--_it;
			return *this;
		}

		Self operator++(int)
		{
			Self tmp(*this);
			--_it;
			return tmp;
		}

		Self& operator--()
		{
			++_it;
			return *this;
		}
		Self operator--(int)
		{
			Self tmp(*this);
			++_it;
			return tmp;
		}
		bool operator==(const Self& rit)
		{
			return _it == rit._it;
		}
		bool operator!=(const Self& rit)
		{
			return _it != rit._it;
		}
	public:
		Iterator _it;//正向迭代器
	};



	template<class T>
	class list
	{
		typedef ListNode<T> Node;
		typedef ListIterator<T, T&, T*> iterator;
		typedef ListIterator<T, const T&, const T*> const_iterator;

		typedef ListReverseIterator<iterator> reverse_iterator;
		typedef ListReverseIterator<const_iterator> const_reverse_iterator;
	public:
		///////////////构造&&析构///////////////////
		list()
		{
			CreateHead();
		}
		list(int n, const T& value)
		{
			CreateHead();
			for (int i = 0; i < n; i++)
			{
				push_back(value);
			}
		}

		//const类型的对象不能调用普通成员函数
		list(const list<T>& L)
		{
			CreateHead();
			auto iter = L.cbegin();
			while (iter != L.cend())
			{
				push_back(*iter);
				++iter;
			}
		}

		template<class Iterator>
		list(Iterator first, Iterator last)
		{
			CreateHead();
			auto iter = first;
			while (iter != last)
			{
				push_back(*iter);
				++iter;
			}
		}

		~list()
		{
			clear();//清空所有有效节点
			delete _head;
			_head = nullptr;
		}

		list<T>& operator=(const list<T>& L)
		{
			if (this != &L)
			{
				clear();//将this指向的链表有效元素清空
				auto iter = L.cbegin();
				while (iter != L.cend())
				{
					push_back(*iter);
					++iter;
				}
			}
			
			return *this;
		}
		///////////////迭代器///////////////////////
		//正向迭代器
		iterator begin()
		{
			return iterator(_head->next);
		}
		iterator end()
		{
			return iterator(_head);
		}

		//const类型的迭代器
		const_iterator cbegin()const
		{
			return const_iterator(_head->next);
		}
		const_iterator cend()const
		{
			return const_iterator(_head);
		}

		//反向迭代器
		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}
		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}

		//const类型的迭代器
		const_reverse_iterator crbegin()const
		{
			return const_reverse_iterator(cend());
		}
		const_reverse_iterator crend()const
		{
			return const_reverse_iterator(cbegin());
		}

		//////////////容量相关//////////////////

		size_t size()const
		{
			auto iter = cbegin();
			size_t count = 0;
			while (iter != cend())
			{
				++count;
				++iter;
			}
			return count;
		}
		bool empty()const
		{
			return _head == _head->next;
		}

		void resize(size_t newsize, const T& value = T())
		{
			size_t oldsize = size();
			if (newsize <= oldsize)
			{
				//将有效元素个数减少至newsize个
				for (size_t i = newsize; i < oldsize; i++)
				{
					pop_back();
				}
			}
			else
			{
				//将有效元素增加至newsize
				for (size_t i = oldsize; i < newsize; ++i)
				{
					push_back(value);
				}
			}
		}

		/////////////////元素访问///////////////////////////

		T& front()
		{
			return *begin();
		}

		const T& front()const
		{
			return *cbegin();
		}

		T& back()
		{
			auto iter = end();
			--iter;
			return *iter;
		}

		const T& back()const
		{
			//auto iter = cend();
			//--iter;
			//return *iter;
			return _head->prev->data;
		}

		//////////////修改////////////////////
		void push_back(const T& value)
		{
			insert(end(), value);
		}

		void pop_back()
		{
			if (empty())
			{
				return;
			}
			auto iter = end();
			--iter;
			erase(iter);
		}

		void push_front(const T& value)
		{
			insert(begin(), value);
		}

		void pop_front()
		{
			erase(begin());
		}

		iterator insert(iterator InsertPos, const T& value)
		{
			Node* newNode = new Node(value);
			Node* pos = InsertPos._pNode;
			newNode->next = pos;
			newNode->prev = pos->prev;
			newNode->prev->next = newNode;
			pos->prev = newNode;

			return newNode;
		}

		iterator erase(iterator Erasepos)
		{
			Node* pos = Erasepos._pNode;
			if (Erasepos == end())
			{
				return end();
			}
			Node* nextPos = pos->next;
			pos->prev->next = pos->next;
			pos->next->prev = pos->prev;
			delete pos;
			return nextPos;
		}

		iterator erase(iterator first, iterator last)
		{
			auto iter = first;
			while (iter != last)
			{
				iter = erase(iter);
			}
			return iter;
		}

		/////////////其他////////////////
		void clear()
		{
			erase(begin(), end());
		}

		void swap(list<T>& L)
		{
			std::swap(_head, L._head);
		}

	private:
		//创建头节点
		void CreateHead()
		{
			_head = new Node();
			_head->next = _head;
			_head->prev = _head;
			_head->data = 0;
		}
	private:
		Node* _head;
	};

	template<class T>
	void PrintList(const list<T>& L)
	{
		auto iter = L.cbegin();
		while (iter != L.cend())
		{
			cout << *iter << " ";
			++iter;
		}
		cout << endl;
	};
}


void Test1()
{
	gyj::list<int> l1;
	gyj::list<int> l2(10, 3);
	gyj::PrintList(l2);

	int array[] = { 1, 2, 3, 4, 5, 6 };
	gyj::list<int> l3(array, array + sizeof(array) / sizeof(array[0]));
	gyj::PrintList(l3);

	gyj::list<int> l4(l3);
	gyj::PrintList(l4);

	l1 = l2;
	gyj::PrintList(l1);
}

void Test2()
{
	int array[] = { 1, 2, 3, 4, 5, 6 };
	gyj::list<int> l(array, array + sizeof(array) / sizeof(array[0]));

	//迭代器方式访问
	auto iter = l.begin();
	while (iter != l.end())
	{
		cout << *iter << " ";
		++iter;
	}
	cout << endl;

	//范围for
	for (auto e : l)
	{
		cout << e << " ";
	}
	cout << endl;

}

void Test3()
{
	int array[] = { 1, 2, 3, 4, 5, 6 };
	gyj::list<int> l(array, array + sizeof(array) / sizeof(array[0]));
	cout << "The size is " << l.size() << endl;
	if (l.empty())
	{
		cout << "empty!" << endl;
	}
	else
	{
		cout << "not empty" << endl;
	}

	l.resize(l.size() - 3);
	gyj::PrintList(l);

	l.resize(10,100);
	gyj::PrintList(l);

	cout << "The first value is" << l.front() << endl;
	cout << "The last value is " << l.back() << endl;
}

void Test4()
{
	gyj::list<int> l;
	l.push_back(1);
	l.push_back(2);
	l.push_back(3);
	l.push_back(4);
	gyj::PrintList(l);

	l.pop_back();
	gyj::PrintList(l);

	l.push_front(0);
	l.push_front(-1);
	gyj::PrintList(l);

	l.pop_front();
	gyj::PrintList(l);

}

void Test5()
{
	int array[] = { 1, 2, 3, 4, 5, 6 };
	gyj::list<int> l(array, array + sizeof(array) / sizeof(array[0]));
	auto iter = l.rbegin();
	while (iter != l.rend())
	{
		cout << *iter << " ";
		++iter;
	}
	cout << endl;
}

int main()
{
	//Test1();
	//Test2();
	//Test3();
	//Test4();
	Test5();
	_CrtDumpMemoryLeaks();
	return 0;
}