#pragma once

template<class T>
struct BSTNode
{
	BSTNode<T>* _left;
	BSTNode<T>* _right;
	T _value;

	BSTNode(const T& value = T())
		:_left(nullptr)
		, _right(nullptr)
		, _value(value)
	{}
};


template<class T>
class BSTree
{
	typedef BSTNode<T> Node;
public:
	BSTree()
		: _root(nullptr)
	{}

	~BSTree()
	{
		DestoryBSTree(_root);
	}

	Node* Insert(const T& value)
	{
		//1、空树
		if (nullptr == _root)
		{
			_root = new Node(value);
			return _root;
		}
		//2、非空
		/*
		 * 1.寻找合法的插入位置，并且保存其双亲（否则无法插入成功）
		 * 2.将新节点插入该位置
		*/
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			parent = cur;
			if (value > cur->_value)
			{
				cur = cur->_right;
			}
			else if (value < cur->_value)
			{
				cur = cur->_left;
			}
			else
			{
				//默认二叉搜索树中没有值相等的节点
				return cur;
			}
		}
		//插入新节点
		cur = new Node(value);
		if (value > parent->_value)
		{
			parent->_right = cur;
		}
		else
		{
			parent->_left = cur;
		}
		return cur;
	}

	bool Erase(const T& value)
	{
		if (nullptr == _root)
		{
			return false;
		}
		//1、找待删除节点在二叉搜索树中的位置,并记录其双亲的位置
		Node* delNode = _root;
		Node* parent = nullptr;
		while (delNode)
		{
			if (value == delNode->_value)
			{
				break;
			}
			else if (value < delNode->_value)
			{
				parent = delNode;
				delNode = delNode->_left;
			}
			else
			{
				parent = delNode;
				delNode = delNode->_right;
			}
		}
		//delNode指向的是待删除节点，parent指向的是delNode的父节点
		if (nullptr == delNode)
		{
			return false;//值为value的节点不存在
		}
		//节点存在，开始删除
		if (nullptr == delNode->_right)
		{
			//此时待删除节点存在两种情况：①只有左子树 ②是一个叶子节点
			if (nullptr == parent)
			{
				_root = delNode->_left;
			}
			else
			{
				//删除的节点不是根节点
				if (delNode == parent->_right)
				{
					//待删除节点是其父节点的右孩子
					parent->_right = delNode->_left;
				}
				else
				{
					parent->_left = delNode->_left;
				}
			}
		}
		else if (nullptr == delNode->_left)
		{
			//待删除节点只有右子树--删除思路类比只有左子树的情况
			if (nullptr == parent)
			{
				_root = delNode->_right;
			}
			else
			{
				if (delNode == parent->_left)
				{
					parent->_left = delNode->_right;
				}
				else
				{
					parent->_right = delNode->_right;
				}
			}
		}
		else
		{
			//待删除节点左右子树均存在
			/*
			*删除思路：
			          1、找到替代的节点，并记录其双亲（替代节点可以是待删除节点左子树的最大值，也可以是右子树的最小值）
					  2、将替代节点的value值赋值给待删除节点
					  3、删除替代节点（此时替代节点可能是一个叶子节点也可能是一个只有右子树的节点，因此直接删除即可）
			*/
			parent = delNode;
			Node* replaceNode = delNode->_right;//找delNode右子树的最小值对应的节点
			while (replaceNode->_left)
			{
				parent = replaceNode;
				replaceNode = replaceNode->_left;
			}

			delNode->_value = replaceNode->_value;
			if (replaceNode == parent->_left)
			{
				parent->_left = replaceNode->_right;
			}
			else
			{
				parent->_right = replaceNode->_right;
			}
			delNode = replaceNode;
		}
		delete delNode;
		delNode = nullptr;
		return true;
	}

	Node* Find(const T& value)
	{
		if (_root)
		{
			Node* cur = _root;
			while (cur)
			{
				if (value > cur->_value)
				{
					cur = cur->_right;
				}
				else if (value < cur->_value)
				{
					cur = cur->_left;
				}
				else
				{
					return cur;
				}
			}
		}
		return nullptr;
	}

	void Inorder()
	{
		Inorder(_root);
		cout << endl;
	}
private:
	void DestoryBSTree(Node* root)
	{
		if (root)
		{
			DestoryBSTree(root->_left);
			DestoryBSTree(root->_right);
			delete root;
			root = nullptr;
		}
	}

	void Inorder(Node* root)
	{
		if (root)
		{
			Inorder(root->_left);
			cout << root->_value << " ";
			Inorder(root->_right);
		}
	}
private:
	Node* _root;
};