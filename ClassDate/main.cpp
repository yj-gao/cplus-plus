#include "date.h"

void Test1()
{
	Date d1(1900,1,1);
	d1.Print();
	Date d2(-1, 2, 90);
	d2.Print();
	Date d3(2000, 2, 29);
	d3.Print();

	//测试 += + - -=
	d1 += 32;
	d1.Print();//d1前后发生改变

	Date tmp = d3 + 32;
	d3.Print(); //d3不变
	tmp.Print();

	d1 += -32;
	d1.Print();

	//测试 前置++ -- 后置++ --
	Date d4(2000, 1, 1);
	--d4;
	d4.Print();
	++d4;
	d4.Print();

	Date tmp1 = d4--;
	d4.Print();
	tmp1.Print();

	Date tmp2 = d4++;
	d4.Print();
	tmp2.Print();

}


void Test2()
{
	//测试 > >= < <=  == !=
	Date d1(2001, 5, 7);
	Date d2(d1);
	if (d1 == d2)
	{
		cout << "d1 == d2" << endl;
	}
	
	d1--;
	if (d1 < d2)
	{
		cout << "d1 < d2" << endl;
	}

	if (d2 > d1)
	{
		cout << "d2 > d1" << endl;
	}

	if (d1 <= d2)
	{
		cout << "d1 <= d2" << endl;
	}

	if (d2 >= d1)
	{
		cout << "d2 >= d1" << endl;
	}

}

void Test3()
{
	//测试日期- 日期
	Date d1(2001, 4, 5);
	Date d2(2001, 4, 1);
	int count1 = d2 - d1;
	int count2 = d1 - d2;
	cout << count1 << endl;
	cout << count2 << endl;
}

int main()
{
	Test1();
	Test2();
	Test3();
	return 0;
}