#include "date.h"

Date::Date(int year, int month, int day)
{
	//检查日期的合法性
	if (year >= 0 && month > 0 && month <= 12 && day > 0 && day <= GetMonthDay(year, month))
	{
		_year = year;
		_month = month;
		_day = day;
	}
	else
	{
		cout << "非法日期，已修改为默认初始日期" << endl;
		_year = 1900;
		_month = 1;
		_day = 1;
	}
}
void Date::Print() const
{
	cout << _year << "-" << _month << "-" << _day << endl;
}

// 拷贝构造函数
// d2(d1)
Date::Date(const Date& d)
{
	_year = d._year;
	_month = d._month;
	_day = d._day;
}

// 赋值运算符重载
// d2 = d3 -> d2.operator=(&d2, d3)
Date&  Date::operator=(const Date& d)
{
	if (this != &d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	return *this;
}

// 获取某年某月的天数
inline int Date::GetMonthDay(int year, int month)
{
	static int dayArray[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	int day = dayArray[month];
	if (2 == month && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))
	{
		day = 29;
	}
	return day;
}

/////////////////////////////////////////////////////////
// 日期+=天数
Date&  Date::operator+=(int day)
{
	if (day < 0)
	{
		*this -= -day;
	}
	else
	{
		_day += day;
		while (_day > GetMonthDay(_year,_month))
		{
			//产生进位，调整为正确的日期格式
			//减掉当月的天数，然后将月+1
			_day -= GetMonthDay(_year, _month);
			++_month;
			//判断月是否越界，月满了的话年+1
			if (_month > 12)
			{
				++_year;
				_month = 1;
			}
		}
	}
	return *this;
}
// 日期+天数
Date  Date::operator+(int day) const
{
	//日期不能发生变化，返回的是日期加上天数的结果（因此无法使用引用返回）
	//可以复用+=
	Date tmp(*this);
	tmp += day;

	return tmp;
}

// 日期-=天数
Date& Date::operator-=(int day)
{
	if (day < 0)
	{
		*this += -day;
	}
	else
	{
		_day -= day;
		while (_day <= 0)
		{
			//月先--，因为借的是上一个月的天数
			--_month;
			if (_month == 0)
			{
				--_year;
				_month = 12;
			}
			_day += GetMonthDay(_year, _month);
		}
	}
	return *this;
}
// 日期-天数
Date Date::operator-(int day) const
{
	//拷贝构造当前对象
	Date tmp(*this);
	//复用-=
	tmp -= day;

	return tmp;
}
///////////////////////////////


// 前置++
Date&  Date::operator++()
{
	*this += 1;
	return *this;
}
// 后置++(int是一个占位符，不起实际作用)
Date  Date::operator++(int)
{
	Date tmp(*this);
	*this += 1;
	return tmp;
}
// 后置--
Date Date::operator--(int)
{
	Date tmp(*this);
	*this -= 1;
	return tmp;
}
// 前置--
Date& Date::operator--()
{
	*this -= 1;
	return *this;
}
/////////////////////////////////////////


// >运算符重载
bool Date::operator>(const Date& d) const
{
	if (_year > d._year)
	{
		return true;
	}
	else if (_year == d._year)
	{
		if (_month > d._month)
		{
			return true;
		}
		else if (_month == d._month)
		{
			if (_day > d._day)
			{
				return true;
			}
		}
	}
	return false;
}
// ==运算符重载
bool Date::operator==(const Date& d) const
{
	return _year == d._year &&
		_month == d._month &&
		_day == d._day;
}
// >=运算符重载
inline bool Date::operator >= (const Date& d) const
{
	return *this > d || *this == d;
}
// <运算符重载
bool Date::operator < (const Date& d) const
{
	return !(*this >= d);
}
// <=运算符重载
bool Date::operator <= (const Date& d) const
{
	return *this < d || *this == d;
}
// !=运算符重载
bool Date::operator != (const Date& d) const
{
	return !(*this == d);
}
//////////////////////////////////////////////////


// 日期-日期 返回天数
int Date::operator-(const Date& d)
{
	Date max = *this;
	Date min = d;
	int flag = 1;
	if (max < min)
	{
		max = d;
		min = *this;
		flag = -1;
	}
	int count = 0;
	while (min != max)
	{
		++min;
		count++;
	}
	return count* flag;
}