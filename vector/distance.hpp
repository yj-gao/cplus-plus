#pragma once

namespace gyj
{
	template<class Iterator>
	size_t distance(Iterator first, Iterator last)
	{
		size_t count = 0;
		while (first != last)
		{
			++count;
			++first;
		}
		return count;
	}
}