#pragma warning(disable:4996)

#include<iostream>
#include<assert.h>

using namespace std;

#include"distance.hpp"

namespace gyj
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;
		typedef T* reverse_iterator;
		vector()
			:start(nullptr)
			, finish(nullptr)
			, end_of_storage(nullptr)
		{}

		vector(size_t n, const T& val = T())
			:start(new T[n])
			, finish(start)
			, end_of_storage(start + n)
		{
			//对申请的空间初始化
			for (size_t i = 0; i < n; i++)
			{
				*finish++ = val;
			}
		}
		vector(int n, const T& val = T())
			:start(new T[n])
			, finish(start)
			, end_of_storage(start + n)
		{
			//对申请的空间初始化
			for (int i = 0; i < n; i++)
			{
				*finish++ = val;
			}
		}


		template<class Iterator>
		vector(Iterator first, Iterator last)
		{
			size_t n = gyj::distance(first,last);

			start = new T[n];
			finish = start;
			end_of_storage = start + n;
			while (first != last)
			{
				*finish = *first;
				++finish;
				++first;
			}
		}

		vector(const vector<T>& v)
			:start(nullptr)
			, finish(nullptr)
			, end_of_storage(nullptr)
		{
			vector<T> tmp(v.cbegin(), v.cend());
			//这里调用的是std命名空间里面提供的swap函数，
			//该函数是通过创建临时变量的方式进行交换，效率太低
			//swap(*this, tmp); 

			//建议使用vector内部实现的swap函数
			this->swap(tmp);
		}

		~vector()
		{
			if (start)
			{
				delete[] start;
				start = nullptr;
				finish = nullptr;
				end_of_storage = nullptr;
			}
		}

		vector<T>& operator=(vector<T> v)
		{
			this->swap(v);
			return *this;
		}

		/////////////////迭代器////////////////////
		iterator begin()
		{
			return start;
		}

		iterator end()
		{
			return finish;
		}

		const_iterator cbegin()const
		{
			return start;
		}

		const_iterator cend()const
		{
			return finish;
		}
		////////////////容量相关///////////////////
		size_t size()const
		{
			return finish - start;
		}

		size_t capacity()const
		{
			return end_of_storage - start;
		}

		bool empty()
		{
			return start == finish;
		}

		void resize(size_t n, const T& val = T())
		{
			size_t oldsize = size();
			if (n > oldsize)
			{
				size_t oldcapacity = capacity();
				if (n > oldcapacity)
				{
					//扩容
					//注意，这里扩容为n而不是以2倍的方式扩容，因为 n有可能大于 2* oldcapacity
					reserve(n);
				}
				for (size_t i = oldsize; i < n; i++)
				{
					start[i] = val;
				}
			}
			finish = start + n;
		}

		void reserve(size_t n)
		{
			size_t oldcapacity = capacity();
			size_t num = size();
			if (n > oldcapacity)
			{
				//申请新空间
				T* tmp = new T[n];
				if (start)
				{
					//拷贝元素
					//这是浅拷贝，在这里使用会导致内存泄露和程序崩溃
					//memcpy(tmp, start, sizeof(T)*num);

					for (size_t i = 0; i < num; i++)
					{
						tmp[i] = start[i];//调用赋值运算符的重载
					}

					//释放旧空间
					delete[] start;
				}
			
				start = tmp;
				finish = start + num;
				end_of_storage = start + n;
			}
		}
		////////////////元素访问////////////////////
		T& operator[](size_t index)
		{
			assert(index < size());
			return start[index];
		}

		const T& operator[](size_t index)const
		{
			assert(index < size());
			return start[index];
		}

		T& front()
		{
			return start[0];
		}
		const T& front()const
		{
			return *begin();
		}

		T& back()
		{
			return *(finish - 1);
		}
		const T& back() const
		{
			reutnr *(finish - 1);
		}
		//////////////////修改相关/////////////////////
		void push_back(const T& val)
		{
			if (finish == end_of_storage)
			{
				reserve(capacity() * 2 + 3);
			}
			*finish++ = val;
		}
		
		void pop_back()
		{
			if (start != finish)
			{
				--finish;
			}
		}

		iterator insert(iterator pos, const T& val)
		{
			if (pos < start || pos > finish)
			{
				return pos;
			}
			if (finish == end_of_storage)
			{
				reserve(capacity() * 2);
			}
			auto iter = finish - 1;
			while (iter >= pos)
			{
				*(iter + 1) = *iter;
				--iter;
			}
			*pos = val;
			++finish;
			return pos;
		}

		iterator erase(iterator pos)
		{
			if (pos < start || pos >= finish)
			{
				return end();
			}
			auto iter = pos + 1;
			while (iter != finish)
			{
				*(iter - 1) = *iter;
				++iter;
			}
			--finish;
			return pos;
		}
		
		void clear()
		{
			finish = start;
		}
		void swap(vector<T>& v)
		{
			std::swap(start,v.start);
			std::swap(finish, v.finish);
			std::swap(end_of_storage, v.end_of_storage);
		}

	private:
		iterator start;
		iterator finish;
		iterator end_of_storage;
	};
}


////////////////自定义类型String///////////////////
class String
{
	friend ostream& operator<<(ostream& _cout, const String& s);
public:
	String(const char* str = "")
	{
		if (nullptr == str)
		{
			str = "";
		}
		_str = new char[strlen(str) + 1];
		strcpy(_str, str);
	}

	String(const String& s)
		:_str(new char[strlen(s._str) + 1])
	{
		strcpy(_str, s._str);
	}

	String& operator=(const String& s)
	{
		if (this != &s)
		{
			char* temp = new char[strlen(s._str) + 1];
			strcpy(temp, s._str);
			delete[] _str;
			_str = temp;
		}
		return *this;
	}

	bool operator==(const String& s)const
	{
		return strcmp(_str, s._str) == 0;
	}

	~String()
	{
		if (_str)
		{
			delete[] _str;
			_str = nullptr;
		}
	}

private:
	char* _str;
};

ostream& operator<<(ostream& _cout, const String& s)
{
	_cout << s._str;
	return _cout;
}

//内置类型的测试
void Test1()
{
	gyj::vector<int> v1;
	gyj::vector<int> v2(10, 2);
	int array[] = { 1, 2, 3, 4, 5 };
	//区间构造
	gyj::vector<int> v3(array, array + sizeof(array) / sizeof(array[0]));
	//拷贝构造
	gyj::vector<int> v4(v3);
	v1 = v3;

	//////测试迭代器////////
	auto iter = v1.begin();
	while (iter != v1.end())
	{
		cout << *iter << " ";
		++iter;
	}
	cout << endl; 
}

//自定义类型的测试
void Test2()
{
	gyj::vector<String> v1;

	gyj::vector<String> v2(5,"Hello");

	gyj::vector<String> v3(v2);
	v1 = v3;

	auto it = v2.begin();
	while (it != v2.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}


//测试容量相关的接口
//内置类型
void Test3()
{
	gyj::vector<int> v(10, 3);
	cout << v.size() << endl;
	cout << v.capacity() << endl;
	if (v.empty())
	{
		cout << "vector v is empty!" << endl;
	}
	else
	{
		cout << "vector v is not empty!" << endl;
	}

	v.resize(2);
	v.resize(20, 800);

	v.reserve(10);
	v.reserve(30);
}
//自定义类型
void Test4()
{
	gyj::vector<String> v(5, "Hello");
	cout << v.size() << endl;
	cout << v.capacity() << endl;
	if (v.empty())
	{
		cout << "vector v is empty!" << endl;
	}
	else
	{
		cout << "vector v is not empty!" << endl;
	}

	//v.resize(2);
	//v.resize(6);

	//v.reserve(7);
	//v.reserve(10);
}

void Test5()
{
	int array[] = { 1, 2, 3, 4, 5 };
	//区间构造
	gyj::vector<int> v(array, array + sizeof(array) / sizeof(array[0]));

	for (size_t i = 0; i < v.size(); i++)
	{
		cout << v[i] << " ";
	}
	cout << endl;

	cout << v.front() << endl;
	cout << v.back() << endl;
}
void Test6()
{
	gyj::vector<String> v(5, "Hello");

	for (size_t i = 0; i < v.size(); i++)
	{
		cout << v[i] << " ";
	}
	cout << endl;

	cout << v.front() << endl;
	cout << v.back() << endl;
}

//测试修改接口
//内置类型
void Test7()
{
	gyj::vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);

	v.pop_back();
	v.pop_back();

	v.insert(find(v.begin(), v.end(), 2), 10);
	v.insert(find(v.begin(), v.end(), 1), 0);

	v.erase(find(v.begin(), v.end(), 10));

	v.clear();
}
//自定义类型
void Test8()
{
	gyj::vector<String> v;
	v.push_back("Hello ");
	v.push_back("World");
	v.push_back("!!!!");
	v.push_back("~~~~~");

	v.pop_back();
	v.pop_back();

	v.insert(find(v.begin(), v.end(), "World"), "Front");
	v.insert(find(v.begin(), v.end(), "Hello "), "Last");

	v.erase(find(v.begin(), v.end(), "~~~~~"));

	v.clear();
}


void Test9(size_t n)
{
	//使用vector定义二维数组，
	//vv中的每一个元素都是一个一维数组vector<int>
	gyj::vector<vector<int>>vv(n);

	//将二维数组每一行中的vector<int>中的元素初始化为1
	for (size_t i = 0; i < n; i++)
	{
		vv[i].resize(i + 1, 1);
	}

	//给杨辉三角除第一列和对角线的所有元素赋值
	for (int i = 2; i < n; i++)
	{
		for (int j = 1; j < i; j++)
		{
			vv[i][j] = vv[i - 1][j] + vv[i - 1][j - 1];
		}
	}

}

#include <vector>
int main()
{

	//Test1();
	//Test2();
	//Test3();
	//Test4();
	//Test5();
	//Test6();
	//Test7();
	Test8();
	_CrtDumpMemoryLeaks();

	return 0;
}