#pragma warning(disable:4996)
#include<iostream>
#include"mystring.h"
#include<assert.h>

gyj::string::string(const char* s)
{
	if (nullptr == s)
	{
		assert(0);
	}
	_size = strlen(s);
	_capacity = _size;
	_str = new char[_capacity+ 1];
	strcpy(_str, s);
}
gyj::string::string(const string& str)
:_str(nullptr)
, _size(0)
, _capacity(0)
{
	string temp(str._str);
	std::swap(_str, temp._str);
	_size = str._size;
	_capacity = str._capacity;
}

gyj::string& gyj::string::operator=(const string& str)
{
	if (this != &str)
	{
		string temp(str._str);
		std::swap(_str, temp._str);
		_size = str._size;
		_capacity = str._capacity;
	}
	return *this;
}

gyj::string::~string()
{
	if (_str)
	{
		delete[] _str;
		_str = nullptr;
	}
}
///////迭代器相关/////////////
gyj::string::iterator gyj::string::begin()
{
	return _str;
}

gyj::string::iterator gyj::string::end()
{
	return _str + _size;
}

///////容量相关//////////////
size_t gyj::string::size()const
{
	return _size;
}
size_t gyj::string::capacity()const
{
	return _capacity;
}

bool gyj::string::empty()const
{
	if (0 == _size)
	{
		return true;
	}
	return false;
}
void gyj::string::resize(size_t n, char c)
{
	if (n > _size)
	{
		if (n > _capacity)
		{
			reserve(n);
		}
		memset(_str + _size, c, n - _size);
	}
	_size = n;
	_str[_size] = '\0';
}
void gyj::string::reserve(size_t n)
{
	if (n > _capacity)
	{
		char* temp = new char[n + 1];
		strcpy(temp, _str);
		delete[] _str;
		_str = temp;
		_capacity = n;
	}
}
///////元素访问相关//////////
char& gyj::string::operator[](size_t index)
{
	assert(index < _size);
	return _str[index];
}
const char& gyj::string::operator[](size_t index)const
{
	assert(index < _size);
	return _str[index];
}
///////修改相关的///////////
void gyj::string::push_back(char c)
{
	if (_size == _capacity)
	{
		//扩容
		reserve(_capacity * 2);
	}
	_str[_size++] = c;
	_str[_size] = '\0';
}
gyj::string& gyj::string::operator+=(char c)
{
	push_back(c);
	return *this;
}
void gyj::string::append(const char* str)
{
	if (_size+strlen(str) >= _capacity)
	{
		//扩容
		size_t newcapacity = _capacity * 2 > _size + strlen(str) ? _capacity * 2 : _size + strlen(str) + 1;
		reserve(newcapacity);
	}
	strcat(_str, str);
	_size += strlen(str);
}
gyj::string& gyj::string::operator+=(const char* str)
{
	append(str);
	return *this;
}
void gyj::string::clear()
{
	_str[0] = '\0';
	_size = 0;
}
void gyj::string::swap(string& s)
{
	std::swap(_str, s._str);
	std::swap(_size, s._size);
	std::swap(_capacity, s._capacity);
}
const char* gyj::string::c_str()const
{
	return _str;
}




// <<重载
std::ostream& gyj::operator<<(std::ostream& _cout, const gyj::string& s)
{
	_cout << s._str;

	return _cout;
}

/////////其他//////////////
bool gyj::string::operator<(const string& s)
{
	int ret = strcmp(_str, s._str);
	if (ret < 0)
	{
		return true;
	}
	return false;
}
bool gyj::string::operator<=(const string& s)
{
	return !(*this>s);
}
bool gyj::string::operator>(const string& s)
{
	int ret = strcmp(_str, s._str);
	if (ret > 0)
	{
		return true;
	}
	return false;
}
bool gyj::string::operator>=(const string& s)
{
	return !(*this < s);
}
bool gyj::string::operator==(const string& s)
{
	int ret = strcmp(_str, s._str);
	if (ret == 0)
	{
		return true;
	}
	return false;
}
bool gyj::string::operator!=(const string& s)
{
	return !(*this == s);
}

// 返回c在string中第一次出现的位置
size_t gyj::string::find(char c, size_t pos) const
{
	for (size_t i = pos; i < _size; i++)
	{
		if (c == _str[i])
		{
			return i;
		}
	}
	return -1;
}
// 返回子串s在string中第一次出现的位置
size_t gyj::string::find(const char* s, size_t pos) const
{
	assert(s);
	assert(pos < _size);
	const char* src = _str + pos;
	while (*src)
	{
		const char* match = s;
		const char* cur = src;
		while (*match && *match == *cur)
		{
			++match;
			++cur;
		}
		if (*match == '\0')
		{
			return src - _str;
		}
		else
		{
			++src;
		}
	}
	return -1;
}
// 在pos位置上插入字符c/字符串str，并返回该字符的位置
gyj::string& gyj::string::insert(size_t pos, char c)
{
	assert(pos <= _size);
	if (_size == _capacity)
	{
		//扩容
		char *newstr = new char[_capacity * 2 + 1];
		strcpy(newstr, _str);
		delete[] _str;
		_str = newstr;
		_capacity *= 2;
	}
	//移数据
	for (int i = _size; i >= (int)pos; --i)
	{
		_str[i + 1] = _str[i];

	}
	_str[pos] = c;
	_size++;

	return *this;

}
gyj::string& gyj::string::insert(size_t pos, const char* str)
{
	size_t len = strlen(str);
	if (_size + len > _capacity)//扩容
	{
		//扩容
		char *newstr = new char[_capacity * 2 + 1];
		strcpy(newstr, _str);
		delete[] _str;
		_str = newstr;
		_capacity *= 2;
	}

	//后移数据
	for (int i = _size; i >= (int)pos; --i)
	{
		_str[len + i] = _str[i];
	}

	//拷贝字符串
	while (*str != '\0')
	{
		_str[pos++] = *str++;
	}
	_size += len;
	return *this;
}

// 删除pos位置上的元素，并返回该元素的下一个位置
gyj::string& gyj::string::erase(size_t pos, size_t len)
{
	assert(pos < _size);

	if (pos + len >= _size)//pos位置之后全为0
	{
		_str[pos] = '\0';
		_size = pos;
	}
	else
	{
		strcpy(_str + pos, _str + pos + len);
		_size -= len;
	}
	return *this;

}


void stringTest1()
{
	gyj::string s1("hello");
	gyj::string s2(s1);
	gyj::string s3;
	s3 = s2;

	std::cout <<s1.size() << std::endl;
	std::cout << s1.capacity() << std::endl;
	std::cout << s1.empty() << std::endl;

	s1.resize(3);
	s2.resize(10);
	s3.resize(15, 'A');

	s1.reserve(4);
	s2.reserve(20);
	s3.reserve(100);

	std::cout << s1[0] << std::endl;
	std::cout << s2[3] << std::endl;
	const gyj::string s4("aaaaaa");
	char ch = s4[3];

	//_CrtDumpMemoryLeaks();
}

void stringTest2()
{
	gyj::string s1("Hello");

	s1.push_back(' ');
	s1.append("World");
	s1 += '!';
	s1 += "~~~~";
	
	std::cout << s1.c_str() << std::endl;

	gyj::string s2("12345");
	s1.swap(s2);
	s1.clear();
	_CrtDumpMemoryLeaks();

}


















#if 0
class String
{
public:
	String(const char* str = "")
	{
		if (nullptr == str)
		{
			str = " ";
		}
		_str = new char[strlen(str) + 1];
		strcpy(_str, str);
	}

	//String(const String& s)
	//	:_str(new char[strlen(s._str)+1])
	//{
	//	strcpy(_str, s._str);
	//}
	//优化版本
	String(const String& s)
		:_str(nullptr)
	{
		String temp(s._str);
		swap(_str, temp._str);
	}

	//String& operator=(const String& s)
	//{
	//	if (this != &s)
	//	{
	//		char* temp = new char[strlen(s._str) + 1];
	//		strcpy(temp, s._str);
	//		delete[] _str;
	//		_str = temp;
	//	}
	//	return *this;
	//}

	//优化版本
	//①
	String& operator=(const String& s)
	{
		if (this != &s)
		{
			String temp(s._str);
			swap(_str, temp._str);
		}
		return *this;
	}
	//②
	//String& operator=(String s)
	//{
	//	swap(_str, s._str);
	//	return *this;
	//}

	~String()
	{
		if (_str)
		{
			delete[] _str;
			_str = nullptr;
		}
	}

private:
	char* _str;
};

void StringTest()
{
	String s1("Hello");
	String s2("World");
	s2 = s1;

	String s3(s1);
}

void TestCopy()
{
	string s1(20, 'A');
	string s2(s1);

	printf("&s1 is %p\n",s1.c_str());
	printf("&s2 is %p\n", s2.c_str());
}
#endif

