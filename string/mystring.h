#pragma once

#include<iostream>

namespace gyj
{
	class string
	{
		friend std::ostream& operator<<(std::ostream& _cout, const gyj::string& s);
	public:
		typedef char* iterator;
	public:
		/////////构造和析构///////////
		string(const char* s = "");
		string(const string& str);
		string& operator=(const string& str);
		~string();
		///////迭代器相关/////////////
		iterator begin();
		iterator end();
		///////容量相关//////////////
		size_t size()const;
		size_t capacity()const;
		bool empty()const;
		void resize(size_t n, char c = '\0');
		void reserve(size_t n);

		///////元素访问相关//////////
		char& operator[](size_t index);
		const char& operator[](size_t index)const;
		///////修改相关的///////////
		void push_back(char c);
		string& operator+=(char c);
		void append(const char* str);
		string& operator+=(const char* str);
		void clear();
		void swap(string& s);
		const char* c_str()const;
		/////////其他//////////////
		bool operator<(const string& s);
		bool operator<=(const string& s);
		bool operator>(const string& s);
		bool operator>=(const string& s);
		bool operator==(const string& s);
		bool operator!=(const string& s);


		// 返回c在string中第一次出现的位置
		size_t find(char c, size_t pos = 0) const;
		// 返回子串s在string中第一次出现的位置
		size_t find(const char* s, size_t pos = 0) const;
		// 在pos位置上插入字符c/字符串str，并返回该字符的位置
		string& insert(size_t pos, char c);
		string& insert(size_t pos, const char* str);


		// 删除pos位置上的元素，并返回该元素的下一个位置
		string& erase(size_t pos, size_t len);
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};

}


extern void stringTest1();
extern void stringTest2();