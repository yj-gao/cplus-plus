#pragma once

#include<deque>

namespace gyj
{
	template<class T, class Container = deque<T>>
	class queue
	{
	public:
		queue()
			:_con()
		{}
		void push(const T& data)
		{
			_con.push_back(data);
		}
		void pop()
		{
			if (empty())
			{
				return;
			}
			_con.pop_front();
		}
		T& front()
		{
			return _con.front();
		}
		const T& front()const
		{
			return _con.front();
		}
		T& back()
		{
			return _con.back();
		}
		const T& back()const
		{
			return _con.back();
		}

		bool empty()const
		{
			return _con.empty();
		}
		size_t size()const
		{
			return _con.size();
		}
	private:
		Container _con;
	};
}