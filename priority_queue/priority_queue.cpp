#include<iostream>
#include<vector>

using namespace std;

namespace gyj
{
	template<class T, class Container = vector<T>, class Compar = less<T>>
	class priority_queue
	{
	public:
		priority_queue()
			:_con()
		{}
		template<class Iterator>
		priority_queue(Iterator first, Iterator last)
			: _con(first,last)
		{
			//将_con中的元素调整为堆
			//从最后一个非叶子结点开始采用向下调整算法
			for (int root = (_con.size() - 2) / 2; root >= 0; root--)
			{
				_AdjustDown(root);
			}
		}

		void push(const T& data)
		{
			_con.push_back(data);
			_AdjustUp(_con.size() - 1);
		}

		void pop()
		{
			if (empty())
			{
				return;
			}
			std::swap(_con.front(), _con.back());
			_con.pop_back();
			_AdjustDown(0);
		}

		bool empty()
		{
			return _con.empty();
		}

		size_t size()
		{
			return _con.size();
		}
		const T& top()const
		{
			return _con.front();
		}

	private:
		void _AdjustDown(int parent)
		{
			size_t child = 2 * parent + 1;
			size_t size = _con.size();
			while (child < size)
			{
				//让child标记左右孩子中较大的一个（注意：判断child的合法性）
				Compar com;
				if (child + 1 < size && com(_con[child], _con[child + 1]))
				{
					child += 1;
				}

				//parent与child比较，确定是否需要调整
				if (com(_con[parent], _con[child]))
				{
					std::swap(_con[parent], _con[child]);
					//更新child与parent，判断是否需要再次进行调整
					parent = child;
					child = 2 * parent + 1;
				}
				else
				{
					return;
				}
			}
		}
		void _AdjustUp(int child)
		{
			int parent = (child - 1) / 2;
			while (child >= 0)
			{
				Compar com;
				if (com(_con[parent], _con[child]))
				{
					std::swap(_con[parent], _con[child]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
				{
					return;
				}
			}
		}
	private:
		Container _con;
	};
};

#include<functional>

void Testpriority_queue()
{
	vector<int> vec{ 1, 2, 4, 6, 788, 99, 0, -5 };
	gyj::priority_queue<int> que(vec.begin(), vec.end());
	cout << que.size() << endl;
	cout << que.top() << endl;

	que.push(10000);
	que.push(10001);

	//gyj::priority_queue<int,vector<int>,greater<int>> que1(vec.begin(), vec.end());
	//cout << que.size() << endl;
	//cout << que.top() << endl;
	que.pop();
	que.pop();
}

#include "Stack.hpp"
void TestStack()
{
	gyj::stack<int> st;
	st.push(1);
	st.push(2);
	st.push(3);
	st.push(4);
	st.push(5);
	st.push(6);
	st.push(7);

	cout << st.size() << "   " << st.top() << endl;
	st.pop();
	st.pop();
	cout << st.size() << "   " << st.top() << endl;
}

#include"Queue.hpp"
void TestQueue()
{
	gyj::queue<double> q;
	q.push(1.1);
	q.push(3.3);
	q.push(1.2);
	q.push(4.1);
	q.push(5.1);
	q.push(6.1);

	cout << q.front() << "   " << q.back() << endl;

	q.pop();
	q.pop();
	cout << q.front() << "   " << q.back() << endl;
}

int main()
{
	//Testpriority_queue();
	//TestStack();
	TestQueue();
	return 0;
}