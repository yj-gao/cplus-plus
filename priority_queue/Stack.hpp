#pragma once

#include<deque>

namespace gyj
{
	template<class T, class Container = deque<T>>
	class stack
	{
	public:
		stack()
			:_con()
		{}
		void push(const T& data)
		{
			_con.push_back(data);
		}

		void pop()
		{
			if (empty())
				return;

			_con.pop_back();
		}

		T& top()
		{
			return _con.back();
		}

		const T& top()const
		{
			return _con.back();
		}


		bool empty()const
		{
			return _con.empty();
		}
		size_t size()const
		{
			return _con.size();
		}
	private:
		Container _con;
	};
}