#pragma once

#include <stdio.h>
#ifdef __cplusplus
extern "C"
{
#endif

	int Add(int left, int right);
	int Sub(int left, int right);
	int Mul(int left, int right);
	int Div(int left, int right);

#ifdef __cplusplus
}
#endif
