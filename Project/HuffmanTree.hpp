#pragma once
#include<vector>
#include<queue>

using std::vector;
using std::priority_queue;


template<class W>
struct HuffmanTreeNode
{
	HuffmanTreeNode<W>* _left;
	HuffmanTreeNode<W>* _right;
	HuffmanTreeNode<W>* _parent;

	W _weight;

	HuffmanTreeNode(W weight)
	{
		_weight = weight;
		_left = nullptr;
		_right = nullptr;
		_parent = nullptr;
	}
};



template<class W>
class HuffmanTree
{
	typedef HuffmanTreeNode<W> Node;
	struct Compare 
	{
		bool operator()(Node* left, Node* right)
		{
			return left->_weight > right->_weight;
		}
	};
public:
	HuffmanTree()
	{
		_root = nullptr;
	}
	HuffmanTree(vector<W>& weights, W& inValid)
	{
		/*
		1、将所有的权值结点看作各自为一棵仅有1个结点的二叉树，并将其放入小堆中
		2、从堆顶连续取出两个结点（一定是最小的两个）分别作为左右子树构建一个新的二叉树，新的二叉树的根节点进堆
		3、循环上述操作，直到小堆中只有一个结点时结束
		*/
		priority_queue<Node*, vector<Node*>, Compare> que;
		for (auto& e : weights)
		{
			if (inValid != e)
			{
				que.push(new Node(e));
			}
		}

		while (que.size() > 1)
		{
			Node* tmp1 = que.top();
			que.pop();
			Node* tmp2 = que.top();
			que.pop();

			Node* parent = new Node(tmp1->_weight + tmp2->_weight);
			parent->_left = tmp1;
			tmp1->_parent = parent;
			parent->_right = tmp2;
			tmp2->_parent = parent;

			que.push(parent);
		}
		_root = que.top();
	}

	~HuffmanTree()
	{
		Destroy(_root);
	}

	Node* getRoot()
	{
		return _root;
	}


private:
	void Destroy(Node* root)
	{
		if (root)
		{
			Destroy(root->_left);
			Destroy(root->_right);
			delete root;
			root = nullptr;
		}
	}
	Node* _root;//标记Huffman树的根节点
};

