#pragma once
#include<string>
#include<vector>

#include"HuffmanTree.hpp"
#include"Common.hpp"

using std::string;
using std::vector;

//组织每个字节对应的权重信息
struct ByteInfo
{
	uch _ch;//字符
	size_t _appearCount;//权重（_ch出现的次数）
	string _chCode;

	ByteInfo(size_t appearCount = 0)
		:_appearCount(appearCount)
	{}

	ByteInfo operator+(ByteInfo& other)
	{
		return ByteInfo(_appearCount + other._appearCount);
	}

	bool operator>(ByteInfo& other)
	{
		return _appearCount > other._appearCount;
	}
	bool operator!=(ByteInfo& other)
	{
		return _appearCount != other._appearCount;
	}
	bool operator==(ByteInfo& other)
	{
		return _appearCount == other._appearCount;
	}
};

class FileCompressHM
{
public:
	FileCompressHM();
	void CompressFile(const string& filePath);
	void UNCompressFile(const string& filePath);
private:
	//获取Huffman编码
	void getHuffmanCode(HuffmanTreeNode<ByteInfo>* root);
	//向压缩文件中写入解压缩需要的头部文件
	void writeHeadInfo(const string& filePath, FILE* fOut);
	//获取文件的后缀名
	string getFilePostfix(const string& filePath);
	void getLine(FILE* fIn, string& strInfo);
public:
	vector<ByteInfo> _fileInfo;
};