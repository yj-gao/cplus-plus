#pragma once

//问题5 --对于非ASCII码表中的字符，如果使用char来进行记录会出现问题的
//因为char的范围是 -128 -- +127，也就是说会有负数的存在
//所以在通过数组下标访问_fileInfo是就会导致程序崩溃
//因此，我们需要将char更换为unsigned char,为了书写方便，将其重定义为uch

typedef unsigned char uch;