#define _CRT_SECURE_NO_WARNINGS
#include<iostream>

#include"FileCompressHuffman.hpp"


using std::cout;
using std::endl;

//初始化_fileInfo
FileCompressHM::FileCompressHM()
{
	_fileInfo.resize(256);
	for (int i = 0; i < 256; i++)
	{
		_fileInfo[i]._ch = i;
	}
}

//压缩
void FileCompressHM::CompressFile(const string& filePath)
{
	/*
	1、打开待压缩文件，获取文件中每个字符出现的次数
	2、以每个字符出现的次数作为权值构建Huffman树
	3、通过Huffman树获取到每个字符的Huffman编码
	4、向压缩文件中写入解压缩所需要的头部文件（主要用于解压缩时构建Huffman树）
	   ①解压缩后的文件名后缀
	   ②字符次数总行数
	   ③字符出现的次数--格式  字符:数字

	5、遍历待压缩文件的每个字符，按照Huffman编码值进行替换，并将替换结果写入到压缩文件中
	*/
	//1 问题6：对于C文件操作接口，有两类打开文件的方式，一类是以文本文件打开，一类是以二进制文件打开
	 //        其中以文本文件打开时是以EOF（-1）来评判是否读到文件末尾的（因为文本文件不会整体出现-1，对于-1它是按照两个字节存储的）
	//         而我们现在的文件是按照二进制的方式处理的，而二进制文件中会存在FF（-1）的情况，因此使用文本打开方式是不正确的
	//         我们需要使用二进制的方式打开文件
	FILE* fIn = fopen(filePath.c_str(), "rb");
	if (nullptr == fIn)
	{
		cout << "打开待压缩文件失败" << endl;
		return;
	}
	uch buffer[1024];
	while (1)
	{
		size_t readSize = fread(buffer, 1, 1024, fIn);
		if (0 == readSize)
		{
			break;
		}
		for (size_t i = 0; i < readSize; i++)
		{
			_fileInfo[buffer[i]]._appearCount++;
		}
	}
	//2 问题点1 ---创建Huffman树时只需要考虑有效权值，
	//          因此需要传递一个无效权值信息作为建立Huffman树的标准
	ByteInfo inValid;
	HuffmanTree<ByteInfo> hmTree(_fileInfo, inValid);

	//3 问题点2 ---在获取Huffman编码时，我们需要先找到叶子节点，然后逆着向上，直到到达根节点
	//           因此，我们的Huffman树需要采用孩子双亲的表示方法，否则我们无法向上查找双亲
	getHuffmanCode(hmTree.getRoot());

	//4
	FILE* fOut = fopen("2.txt", "wb");
	writeHeadInfo(filePath, fOut);
	
	//5  问题点3 ---由于在第一步统计字符出现次数的时候已经将文件遍历一遍了，此时文件指针处于文件内容的末尾
	//                需要使用fseek函数将文件指针复位到文件的起始位置再进行读取，否则什么内容也读取不到
	/*
	fseek函数的参数，第一个参数：想要操作的文件的文件流指针，
	                 第二个参数：偏移量
					 第三个参数：SEEK_SET:回到文件起始位置
					             SEEK_CUR：在当前位置
								 SEEK_END：回到文件的末尾位置
				   第二个参数和第三个参数搭配使用，可以让文件指针处于 第三个参数值 + 偏移量处
	*/
	fseek(fIn, 0, SEEK_SET);

	uch bits = 0;
	int bitCount = 0;
	while (1)
	{
		size_t readSize = fread(buffer, 1, 1024, fIn);
		if (0 == readSize)
		{
			break;
		}
		for (size_t i = 0; i < readSize; i++)
		{
			string& strCode = _fileInfo[buffer[i]]._chCode;
			for (size_t j = 0; j < strCode.size(); j++)
			{
				bits <<= 1;
				if (strCode[j] == '1')
				{
					bits |= 1;
				}
				bitCount++;
				if (8 == bitCount)
				{
					fputc(bits, fOut);
					bits = 0;
					bitCount = 0;
				}
			}
		}
	}
	//最后一次bits中的8个比特位可能没有填充满，需要单独处理
	if (bitCount > 0 && bitCount < 8)
	{
		bits <<= (8 - bitCount);
		fputc(bits, fOut);
	}

	fclose(fIn);
	fclose(fOut);
}


void FileCompressHM::getHuffmanCode(HuffmanTreeNode<ByteInfo>* root)
{
	if (nullptr == root)
	{
		return;
	}
	getHuffmanCode(root->_left);
	getHuffmanCode(root->_right);

	if (nullptr == root->_left && nullptr == root->_right)
	{
		//达到叶子节点
		string& chCode = _fileInfo[root->_weight._ch]._chCode;
		HuffmanTreeNode<ByteInfo>* cur = root;
		HuffmanTreeNode<ByteInfo>* parent = cur->_parent;
		while (parent)
		{
			if (cur == parent->_left)
			{
				chCode += '0';
			}
			else
			{
				chCode += '1';
			}
			cur = parent;
			parent = cur->_parent;
		}
		reverse(chCode.begin(), chCode.end());
	}
}

void FileCompressHM::writeHeadInfo(const string& filePath, FILE* fOut)
{
	//1、获取文件的后缀名
	string headInfo;
	headInfo += getFilePostfix(filePath);
	headInfo += '\n';
	//2、获取字符次数总行数
	size_t appearLineCount = 0;
	string lineInfo;
	for (auto& e : _fileInfo)
	{
		if (e._appearCount == 0)
		{
			continue;
		}
		
		lineInfo += e._ch;
		lineInfo += ':';
		lineInfo += std::to_string(e._appearCount);
		lineInfo += '\n';
		appearLineCount++;
	}
	headInfo += std::to_string(appearLineCount);
	headInfo += '\n';



	//tip!!!
	//headInfo += lineInfo;


	fwrite(headInfo.c_str(), 1, headInfo.size(), fOut);
	fwrite(lineInfo.c_str(), 1, lineInfo.size(), fOut);
}

string FileCompressHM::getFilePostfix(const string& filePath)
{
	return filePath.substr(filePath.find_last_of('.') + 1);
}

//解压缩
void FileCompressHM::UNCompressFile(const string& filePath)
{
	/*
	1、从压缩文件中获取源文件的后缀
	2、从压缩文件中获取字符次数总行数
	3、获取每个字符出现的次数
	4、重建Huffman树
	5、解压缩
	*/
	FILE* fIn = fopen(filePath.c_str(), "rb");
	if (nullptr == fIn)
	{
		cout << "压缩文件打开失败" << endl;
		return;
	}
	//1
	string unCompressInfo("222.");
	string strInfo;
	getLine(fIn, strInfo);
	unCompressInfo += strInfo;
	//2
	strInfo = "";
	getLine(fIn, strInfo);
	size_t  lineCount = atoi(strInfo.c_str());

	//3   问题4：读取换行这个字符（\n）出现的次数时需要进行特殊处理
	for (size_t i = 0; i < lineCount; i++)
	{
		strInfo = "";
		getLine(fIn, strInfo);
		//注意：对于读取到的换行符需要特殊处理(重点！！！！！)
		if ("" == strInfo)
		{
			strInfo += '\n';
			//继续读取下一行，因为对于\n来说，在文件中实际是按照两行来存储出现频次信息的
			//（第一行是一个换行符）
			//第二行是----->  :xxx(出现的频次)
			getLine(fIn, strInfo);
		}
		//注意！！！！！string中每一个字符是char类型的，我们这里作为下标访问时一定要转换为unsigned char
		uch ch = strInfo[0];
		_fileInfo[ch]._appearCount = atoi(strInfo.substr(2).c_str());
	}
	//4
	HuffmanTree<ByteInfo> ht(_fileInfo, ByteInfo());


	//5
	FILE* fOut = fopen(unCompressInfo.c_str(), "wb");

	uch buf[1024];
	
	size_t  fileSize = 0;
	HuffmanTreeNode<ByteInfo>* cur = ht.getRoot();
	while (1)
	{
		size_t readSize = fread(buf, 1, 1024, fIn);
		if (0 == readSize)
		{
			break;
		}
		
		for (size_t i = 0; i < readSize; i++)
		{
			uch bits = buf[i];
			
			for (size_t j = 0; j < 8; j++)
			{
				if (bits & 0x80)
				{
					cur = cur->_right;
				}
				else
				{
					cur = cur->_left;
				}
				bits <<= 1;
				
				if (nullptr == cur->_left && nullptr == cur->_right)
				{
					fputc(cur->_weight._ch, fOut);
					cur = ht.getRoot();
					fileSize++;
					if (fileSize == ht.getRoot()->_weight._appearCount)
					{
						break;
					}
				}
			}
		}
	}
	fclose(fIn);
	fclose(fOut);
}

void FileCompressHM::getLine(FILE* fIn, string& strInfo)
{
	while (!feof(fIn))
	{
		uch ch = fgetc(fIn);

		if (ch == '\n')
			break;

		strInfo += ch;
	}
}



